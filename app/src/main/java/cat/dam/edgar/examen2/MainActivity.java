package cat.dam.edgar.examen2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private TextView tv_num, tv_winCounter;
    private Button btn_send;
    private ArrayList<ToggleButton> binaryButtons;
    private LinearLayout ll_tbtns;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVariables();
        setListeners();
        startGame();
    }

    private void setVariables()
    {
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_winCounter = (TextView) findViewById(R.id.tv_winCounter);
        btn_send = (Button) findViewById(R.id.btn_send);
        ll_tbtns = (LinearLayout) findViewById(R.id.ll_binaryButtons);
        setToggleBtn();
    }

    private void setToggleBtn()
    {
        binaryButtons = new ArrayList<>();
        ToggleButton tbtn;
        int nButtons = ll_tbtns.getChildCount();

        for (int i = 0; i < nButtons; i++) {
            tbtn = (ToggleButton) ll_tbtns.getChildAt(i);
            binaryButtons.add(tbtn);
        }
    }

    private void setListeners()
    {
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int binaryNum, decimalNum;

                decimalNum = Integer.parseInt(tv_num.getText().toString());
                binaryNum = calcBinary();

                if (decimalNum == binaryNum) correct();
                else {
                    String zero = "0";
                    tv_winCounter.setText(zero);
                    startGame();
                }
            }
        });
    }

    private int calcBinary()
    {
        int result = 0, i = 3;

        for (ToggleButton tbtn: binaryButtons) {
            if (tbtn.isChecked()) result += Math.pow(2, i);
            i--;
        }

        return result;
    }

    private void startGame()
    {
        Random r = new Random();
        int count = r.nextInt(16);

        tv_num.setText(Integer.toString(count));
    }

    private void correct()
    {
        int onRoll;

        onRoll = (Integer.parseInt(tv_winCounter.getText().toString()))+1;

        if(onRoll == 10) {
            win();
        } else {
            tv_winCounter.setText(Integer.toString(onRoll));
            startGame();
        }
    }

    private void win()
    {
        Intent intent = new Intent(MainActivity.this, Winner.class);
        startActivity(intent);
    }
}